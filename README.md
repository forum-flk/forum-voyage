# Projet Forum : Voyage

L'objectif de ce projet va être de concevoir et mettre en place la base de données pour une application de forum de voyage.

## Description du projet
---
&nbsp;
## Choisir une thématique pour votre forum

Nous avons choisi de prendre comme thème le voyage. Cette thematique nous faisais envie à tous les trois\.

Voici la grille d'évaluation de notre projet:\
<img src="./public/image/grilleEvaluation.png" width="auto" height="300" />

 
## I/Conception
---
Diagramme des cas d'utilisation (use-case diagram) : représentation des possibilités d'interaction entre le système et les acteurs (intervenants extérieurs au système), c'est-à-dire de toutes les fonctionnalités que doit fournir le système\.
>Création du Use case en diagramme UML: grâce à starUML, qui nous permet de lister toutes les fonctionnalités qui seront possibles sur le forum

<img src="./public/image/UseCaseDiagramForum.jpg" width="auto" height="300" />

&nbsp;

## II/Créer 3 ou 4 user stories pour certaines fonctionnalités
---
&nbsp;

#### 1. ***User Stories: un client du forum non identifié***

- En tant que { visiteur du forum de site } rôle utilisateur
- je veux { pouvoir chercher des informations sans m’inscrire } besoin action
- afin de { pouvoir trouver des réponses sur le forum } bénéfice valeur métier

En tant que visiteur d’un forum de voyage, je veux pouvoir chercher des informations concernant mon voyage afin de pouvoir trouver des réponses sur le site.

>fonctionnalités :Recherché un mot clé\

&nbsp;
#### 2. ***Titre: un client du forum de voyage veut se créer un compte***

- en tant que { utilisateur non connecté } rôle utilisateur,
- je veux { pouvoir me créer un profil } besoin action
- afin de { pouvoir interagir sur le forum } bénéfice valeur métier

En tant que client d’un forum de voyage, je veux pouvoir me connecter afin de pouvoir interagir sur le site.

>Fonctionnalité : Authentification\
- Scénario : Authentification avec un compte valide,
- Étant donné que je dispose d’un compte utilisateur « bloup »,
- Quand j’accède à la page d’authentification /auth.html,
- Et que je saisis mon identifiant « blip » dans le champ « Login »,
- Et que je saisie mon mot de passe dans le champ « Password »,
- Et que je clique sur le bouton « Connexion » du formulaire,
- Alors je suis authentifié sur le site,
- Et je suis redirigé vers la page « Mon compte » à l’URL /mon-compte.html.

&nbsp;
#### 3. ***Titre: Utilisateur Authentifié***

- En tant  que { utilisateur connecté }
- Je veux { pouvoir ajouter un Topic }
- afin de {rajouter un sujet , une question autour de la thématique du Forum qui sera visible par la communauté afin qu'elle puisse répondre/ discuter autour de mon Topic }

>Fonctionnalité : Ajouter un Topic

&nbsp;
## III/Créer 3 ou 4 maquettes fonctionnelles de l'application
---
<img src="./public/image/userstorieLing.jpg" width="auto" height="200" />
<img src="./public/image/userstorieFanny.jpg" width="auto" height="200" /> 
<img src="./public/image/userstoriesKamel1.jpg" width="auto" height="200" />
<img src="./public/image/userstoriesKamel2.jpg" width="auto" height="200" /> 

&nbsp;

## IV/Identifier les "entités" de votre forum (ce qui persistera en base de données, les tables):\Sous forme d'un diagramme de classes
---
<img src="./public/image/database.jpg" width="auto" height="300"/>\

&nbsp;

## V/Entities, Repository
---
Dans forum-voyage:
>On a créer un dossier Entities:
- avec à l'intérieur un fichier class par table qui contiendra, dans le constructor, les propriétés.

>Ensuite un dossier Repository pour chaques Entities :\
  entitIesRepo : 
- findAll()
- findById()
- add()
- update()
- delete()

>Chacun à réalisé une entities et un repository.\
On a créé :
- des script(s) SQL des différentes tables et un jeu de données.
- un projet node.js avec mysql2 puis coder un repository/DAO pour chacune des tables/**entités**
- une application express et quelques routes pour les repository.
  
**scripts SQL des tables (expemples)**:

<img src="./public/image/exempleSql.png" width="auto" height="200" />
<img src="./public/image/exempleTopic.png" width="auto" height="200" />

---

**le lien vers notre déploiement Heroku**:https://app-forum-voyage.herokuapp.com/
**le lien vers notre depôt GITLAB**:https://gitlab.com/forum-flk

---

## Projet signé: Ling, Kamel et Fanny Team FLK
---