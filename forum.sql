DROP TABLE IF EXISTS membres;
CREATE TABLE `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(128) NOT NULL,
  `mdp` varchar(32) NOT NULL,
  `email` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4;
DROP TABLE IF EXISTS categories;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4;
DROP TABLE IF EXISTS topics;
CREATE TABLE `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` VARCHAR (60) NOT NULL,
  PRIMARY KEY (`id`),
  `MembreId` int,
  KEY `FK_MembreTopic` (`MembreId`),
  CONSTRAINT `FK_MembreTopic` FOREIGN KEY (`MembreId`) REFERENCES `membres`(`id`) ON DELETE
  SET
    NULL ON UPDATE
  SET
    NULL,
    `CategorieId` int,
    KEY `FK_CategorieTopic` (`CategorieId`),
    CONSTRAINT `FK_CategorieTopic` FOREIGN KEY (`CategorieId`) REFERENCES `categories`(`id`) ON DELETE
  SET
    NULL ON UPDATE
  SET
    NULL
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4;
DROP TABLE IF EXISTS messages;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texte` text NOT NULL,
  PRIMARY KEY (`id`),
  `MembreId` int,
  KEY `FK_MembreMessage` (`MembreId`),
  CONSTRAINT `FK_MembreMessage` FOREIGN KEY (`MembreId`) REFERENCES `membres`(`id`) ON DELETE
  SET
    NULL ON UPDATE
  SET
    NULL,
    `TopicId` int,
    KEY `FK_TopicMessage` (`TopicId`),
    CONSTRAINT `FK_TopicMessage` FOREIGN KEY (`TopicId`) REFERENCES `categories`(`id`) ON DELETE
  SET
    NULL ON UPDATE
  SET
    NULL
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4;
INSERT INTO
  membres (pseudo, mdp, email)
VALUES
  ('Armand', '2345', 'Armand@gmail.com'),
  ('Hebert', '1345', 'Herbert@gmail.com'),
  ('Ribeiro', '6785', 'Ribeiro@gmail.com'),
  ('Savary', '5734', 'kdkdkdkd@gmail.com');
INSERT INTO
  categories (nom)
VALUES
  ('Afrique'),('Amerique'),('Asie'),('Europe'),('Oceanie');
INSERT INTO
  topics (titre, MembreId, CategorieId)
VALUES
  ('Quand partir au Etats-Unis', 1, 2),
  ('Quand partir au Maroc', 2, 1),
  ('Quand partir en Angleterre', 2, 4);
INSERT INTO
  messages (texte, MembreId, TopicId)
VALUES
  ('Super !', 1, 2),
  ('Je viens de revenir', 2, 1),
  ('Formidable', 2, 3);