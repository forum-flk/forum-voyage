import request from "supertest";

import { server } from "../src/server";

describe('API Categories routes', () => {

    it('should return category list on GET', async () => {
        let response = await request(server)
            .get('/api/forum/cat/show/all')
            .expect(200);
      
        expect(response.body).toContainEqual({
            id: expect.any(Number),
            nom: expect.any(String)
            
        });
    });
});