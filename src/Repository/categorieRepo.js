
import {Categories} from '../Entity/Categories';
import {connection} from './bddConnector';

export class CategoriesRepository {

    /**
     * 
     * @param {string} category 
     * @returns {Promise<Categories[]>}
     */
    
    static async findByCategory () {

        const [rows] = await connection.execute('SELECT * FROM categories');
        const categories = [];
        for (const row of rows) {
            let category = new Categories(row.nom, row.id);
            categories.push(category);
        }
    return categories;
    } 

}

