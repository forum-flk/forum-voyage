import { Membres } from '../Entity/Membres';
import { connection } from './bddConnector';

export class MembresRepository {

    static async findByMembre() {

        const [rows] = await connection.execute('SELECT * FROM membres');
        const membres = [];
        for (const row of rows) {
            let membre = new Membres(row.pseudo, row.mdp, row.email, row.id);
            membres.push(membre)
        }
        return membres;
    }

    static async addMembre(membre) {
        await connection.execute('INSERT INTO membres (pseudo, mdp, email) VALUES (?, ?, ?)', [membre.pseudo, membre.mdp, membre.email]);
    }

    static async updateMembre(dataUpdate) {
        await connection.execute('UPDATE membres SET pseudo=?, mdp=?, email=? WHERE id=?', [dataUpdate.pseudo, dataUpdate.mdp, dataUpdate.email, dataUpdate.id]);

    }
    static async deleteMembre(id) {
        await connection.execute('DELETE FROM membres WHERE id=?', [id]);
    }

}
