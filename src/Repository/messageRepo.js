import { Message } from '../Entity/Messages';
import { connection } from './bddConnector';

/**
 * Fonction qui fait une requête SQL avec mysql2 et transforme le 
 * résultat de 
 */
export class MessagesRepository {

    //FONCTIONNE    
    async findById(id) {
        const [rows] = await connection.execute('SELECT * FROM messages WHERE id=?', [id]);
        let messageId = [];
        for (const row of rows) {
            let instance = new Message(row.texte, row.MembreId, row.TopicId);
            messageId.push(instance);
        }
        return messageId;
    }

    //FONCTIONNE
    async findAllMessages() {

        const [rows] = await connection.execute('SELECT * FROM messages');
        const messages = [];
        for (const row of rows) {
            let instance = new Message(row.texte, row.MembreId, row.TopicID, row.id);
            messages.push(instance);

        }
        return messages;
    }
//FONCTIONNE
    async addId(msg) {

        await connection.execute('INSERT INTO messages (texte,MembreId,TopicId) VALUES (?,?,?)', [msg.texte, msg.MembreId, msg.TopicId]);
    }

//FONCTIONNE
    async updateMessage(up) {

        await connection.execute('UPDATE messages SET texte=?, MembreId=?, TopicId=? WHERE id=?', [up.texte, up.MembreId, up.TopicId, up.id]);
    }
//FONCTIONNE
    async deleteId(id) {

        await connection.execute('DELETE FROM messages WHERE id=?', [id]);

    }
}