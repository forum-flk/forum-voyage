import {Topics} from '../Entity/Topics';
import {connection} from './bddConnector';
 
 
export class topicRepository
{
 


//======================================== Method FindAllTopic ok ====================================//

    /**
     * Find every rows in topics table
     * @returns 
     */
        async findAll() {
 
 
       const [rows] = await connection.execute('SELECT * FROM topics ');
       const topics = [];
       for (const row of rows) {
           let topic = new Topics(row.titre, row.MembreId, row.categorieId,row.id);
           topics.push(topic);
  
       }
       return topics;
   }


//======================================== Method FindById ok ====================================//

    

    /**
     * Find a row by his id in the topic table
     * @param {*} id 
     * @returns 
     */

    
     async findById(id) {
        const [rows] = await connection.execute(`SELECT * FROM topics WHERE Id=?`, [id]);
        let topicId = [];
        for (const row of rows) {
            let instance = new Topics (row.titre, row.MembreId, row.CategorieId);
            topicId.push(instance);
        }
        return topicId
    }


//======================================== Method Add ok ====================================//


     /**
     * add a row in the topic table via an object
     * @param {Object} topic
     */

    async add(topic) {
        const [rows] = await connection.execute(`INSERT INTO topics (titre, MembreId, CategorieId) VALUES(?, ?, ?)`, [topic.titre, topic.MembreId, topic.CategorieId]);
    };



//======================================== Method Update ====================================//


   
    async update(topic) {
        const [rows] = await connection.execute(`UPDATE topics SET titre=?, MembreId=?, CategorieId=? WHERE id=?`, [topic.titre, topic.MembreId, topic.CategorieId, topic.id]);
    };





//======================================== Method Delete ====================================//


 
    async delete(id) {
        const [rows] = await connection.execute(`DELETE FROM topics WHERE id=?`, [id]);
    };


  
}
 

