
import express from "express";
import { topicController } from "./controller/topic-controller";
import { messageController } from "./controller/message-controller";
import { categoriesController } from "./controller/category-controller";
import { membresController } from "./controller/membre-controller";

export const server = express();

server.use(express.json());

server.use('/api/forum/topic',topicController);

server.use('/api/forum/message', messageController);

server.use('/api/forum/cat', categoriesController);

server.use('/api/forum/user', membresController);
