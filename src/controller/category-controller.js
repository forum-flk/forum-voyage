import { Router } from "express";
import { CategoriesRepository, findByCategory } from "../Repository/categorieRepo";

export const categoriesController = Router();

categoriesController.get('/show/all', async(req, res) => {
    let categories = await CategoriesRepository.findByCategory()
    res.json(categories);
})
