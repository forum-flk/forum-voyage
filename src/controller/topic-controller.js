import { Router } from "express";
import { topicRepository } from "../Repository/topicRepo";

export const topicController = Router();


//======================================== Router FindAll ok =======================================//
    
topicController.get("/", async (req, res)=>{
try {
        let data = await new topicRepository().findAll();
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})





//======================================== Router FindById OK ====================================//


topicController.get('/:id', async (req, res)=>{
    try {
        let data = await new topicRepository().findById(req.params.id);
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})






//========================================  Router add ok   ====================================//



topicController.post('/add', async (req, res)=>{
    try {        
        await new topicRepository().add(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


//========================================  Router update  ====================================//


topicController.put('/update', async (req, res)=>{
    try {
        await new topicRepository().update(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


//========================================  Router Delete  ====================================//



topicController.delete('/delete/:id', async (req, res)=>{
    try {
        await new topicRepository().delete(req.params.id);
        res.end();
    } catch (error) {
        res.status(500).json({
            message: 'Server Error'
        })
    }
})
