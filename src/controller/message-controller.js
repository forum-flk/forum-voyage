import { Router } from "express";
import { MessagesRepository } from "../Repository/messageRepo";


export const messageController = Router();

//FONCTIONNE
messageController.get('/:id', async (req, res) => {
    try {
        let data = await new MessagesRepository().findById(req.params.id);
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

//FONCTIONNE
messageController.get('/show/all', async (req, res) => {
    const all = await new MessagesRepository().findAllMessages();
    res.json(all)
})

//FONCTIONNE
messageController.put('/update', async (req, res) => {
    await new MessagesRepository().updateMessage(req.body);
    res.end();
})
//FONCTIONNE
messageController.post('/add', async (req, res) => {

    await new MessagesRepository().addId(req.body);
    res.end();
})
//FONCTIONNE
messageController.delete('/delete/:id', async (req, res) => {

    await new MessagesRepository().deleteId(req.params.id);
    res.end();
})


