
import {Router} from "express";
import {MembresRepository} from "../Repository/membreRepo";

export const membresController = Router();


membresController.get('/show/all', async(req, res)=>{
    let data = await MembresRepository.findByMembre()
    res.json(data);
})

membresController.post('/add', async(req, res)=>{
    await MembresRepository.addMembre(req.body)
    res.end();

})

membresController.put('/update', async(req, res)=> {
    await MembresRepository.updateMembre(req.body)
    res.end();
})

membresController.delete('/delete/:id', async(req, res)=>{
    await MembresRepository.deleteMembre(req.params.id)
    res.end();
})