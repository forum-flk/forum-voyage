export class Membres {
    id;
    pseudo;
    mdp;
    email;
 /**
 *
 * @param {String} pseudo
 * @param {any} mdp
 * @param {any} email
 */
    constructor(pseudo, mdp, email, id) {
        this.id = id;
        this.pseudo = pseudo;
        this.mdp = mdp;
        this.email = email;
    }
 }
 