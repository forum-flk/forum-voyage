export class Message {
    id;
    texte;
    MembreId;
    TopicId;
    /**
     * @param {number} id         
     * @param {String} MembreId
     * @param {String} texte
     * @param {number} topicId
     */
    constructor(texte, MembreId, TopicId,id) {
        this.id = id;
        this.MembreId = MembreId;
        this.texte = texte;
        this.TopicId = TopicId;
    }
}