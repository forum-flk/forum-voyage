 
export class Topics {
    id;
    titre;
    membreId;
    categorieId;
  
     constructor(paramTitre, paramMembreId, paramCategorieId, paramId) {
        this.titre = paramTitre;
        this.membreId = paramMembreId;
        this.categorieId = paramCategorieId;
        this.id = paramId;
    }
 }
 